const MongoClient = require("mongodb").MongoClient;
require('dotenv').config();
var connection = null;
var db = null;

function connect(callback){
    if(connection) return callback(null, db); //isto é, se já existir uma conexão estabelecida devolve essa conexão
    MongoClient.connect(process.env.MONGO_CONNECTION, (err, conn) => {
        if(err){
            return callback(err, null);
        }
        else{
            connection = conn;
            db = conn.db(process.env.DATABASE_NAME);
            return callback(null, db);
        }
    })
}

function disconnect(){
    if(!connection) return true; //se a conexão já estiver fechada devolva true

    connection.close();
    connection = null;
    return true;
}

module.exports = {connect, disconnect}