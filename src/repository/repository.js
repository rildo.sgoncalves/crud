const ObjectId = require("mongodb").ObjectId;
const mongodb = require("../config/mongodb");

function findAll(callback){
    mongodb.connect((err, db) => {
        if(err)console.log(err);
        customer = db.collection("customers").find().toArray(callback);
    })
}

function insert(customer, callback){
    mongodb.connect((err, db) => {
        db.collection("customers").insert(customer, callback);
    })
}

function findOne(id, callback){
    mongodb.connect((err, db) => {
        db.collection("customers").findOne(new ObjectId(id), callback);
    })
}

function update(id, customer, callback){
    mongodb.connect((err, db) => {
        db.collection("customers").update({_id: new ObjectId(id)}, customer, callback);
    })
}

function deleteOne(id, callback){
    mongodb.connect((err, db) => {
        db.collection("customers").deleteOne({_id: new ObjectId(id)}, callback);
    })
}

function disconnect(){
    return mongodb.disconnect();
}

module.exports = {findAll, insert, findOne, update, deleteOne, disconnect}