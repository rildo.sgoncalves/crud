const test = require('tape');
const repository = require('./repository');

function runTests(){

    var id = null;

    test('Repository find all customers', (t) => {
        repository.findAll((err, customers) => {
            if(customers && customers.length > 0) id = customers[0]._id;
            //test.skip(!err && customers && customers.length > 0, "All customers returned");
            //t.skip();
            t.assert(!err && customers && customers.length > 0, "All customers returned");
            t.end();
        });
    })

    test('Repository find one customer', (t) => {
        if(!id){
            //test.skip(false, "Movie by Id Returned");
            //t.skip();
            t.assert(false, "Movie by Id Returned");
            t.end();
            return;
        }

        repository.findOne(id, (err, customer) => {
            t.assert(!err&&customer, "Customer by id returned");
            t.end();
        });
    })

    //testar outras funções do banco...

    test('Repository Disconnect', (t) => {
        t.assert(repository.disconnect(), "Disconnect OK");
        t.end();
    })
}

module.exports = {runTests}