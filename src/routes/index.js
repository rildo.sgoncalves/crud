const repository = require('../repository/repository');
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  repository.findAll((err, customer) => {
    if(err){return console.log(err)};
    res.render('index', {customer});
  })
});

/* GET new page. */
router.get('/new', function(req, res, next) {
  res.render('new', { title: 'Cadastro de Cliente', customer: {}, action:'/new' });
});

/* POST new page. */
router.post('/new', function(req, res, next){
  const nome = req.body.nome
  const idade = parseInt(req.body.idade);
  const uf = req.body.uf
  repository.insert({nome, idade, uf}, (err, result) => {
    if(err){return console.log(err)}
    res.redirect('/?new=true')
  })
})

/* Get edit page. */
router.get('/edit/:id',function(req, res, next){
  var id = req.params.id
  repository.findOne(id, (e, customer) => {
    if(e){return console.log(e)}
    console.log(customer.nome)
    res.render('new', {title:'Edição de Cliente', customer: customer, action:'/edit/' + customer._id})
  })
})

/**POST edit page. */
router.post('/edit/:id', function(req, res){
  const id = req.params.id
  const nome = req.body.nome
  const idade = parseInt(req.body.idade)
  const uf = req.body.uf
  repository.update(id, {nome, idade, uf}, (e, result) => {
    if(e){return console.log(e)}
    res.redirect('/?edit=true')
  })
})

/**GET delete page. */
router.get('/delete/:id', function(req, res){
  var id = req.params.id
  repository.deleteOne(id, (e, r) => {
    if(e){return console.log(e)}
    res.redirect('/?delete=true')
  })
})

module.exports = router; 
